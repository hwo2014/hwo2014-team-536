package visualization;

import java.awt.*;
import java.awt.geom.Line2D;

import javax.swing.*;

import java.util.concurrent.TimeUnit;

import javax.swing.JPanel;

public class Painter implements Runnable {
	
public void run() {
	
	visual rects = new visual();
    JFrame frame = new JFrame("HWO corner visualization");
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.add(rects);
    frame.setSize(600, 480);
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);
    int tempTick=-1;

	do{

		try {
	    	TimeUnit.MILLISECONDS.sleep(50);
	    	if(noobbot.Global.gameTick!=tempTick) rects.repaint();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}while(true);
	
}

}

@SuppressWarnings("serial")
class visual extends JPanel {
	
	private RenderingHints rh = new RenderingHints(
			RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	private boolean thingsSet = false;
	private int wWidth;
	private int wHeight;
	private Image dbImage;
	private Graphics dbg;
	private Composite originalComposite;


	private int[] car;
	
	public void carXY(){
		try{
		int i;
		
		for(i= 0; i<noobbot.Global.players.size(); i++){
			if(noobbot.Global.players.get(i).stupid()==false) break;
		}
		int[] temp=noobbot.Global.players.get(i).carXY();
		if(temp!=null){
			car=temp;
		}
		}catch(Exception e){		}
	}


	public void paintComponent(Graphics g) {

		Graphics2D g2d = (Graphics2D) g;

		if (thingsSet == false) {
			wWidth = this.getSize().width;
			wHeight = this.getSize().height;
			setThings(wWidth, wHeight, g2d.getComposite());

		}

		g2d.setRenderingHints(rh);

		g2d.setComposite(originalComposite);

		g2d.setPaint(Color.gray);
		g2d.fillRect(0, 0, wWidth, wHeight);

		
		carXY();
		if(car!=null) {
			
			if(car[6]==0 && car[5]==0){
				g2d.setPaint(new Color(0,150,0));
			}else{
				g2d.setPaint(car[5]==0 ? Color.green : Color.red);
			}
			

			int x1=wWidth/2+car[0];
			int y1=wHeight/2-car[1];
			
			int x2=wWidth/2+car[2];
			int y2=wHeight/2-car[3];
			
			g2d.setStroke(new BasicStroke(car[4]));
		
		   Line2D lin = new Line2D.Float(x1, y1, x2 ,y2);
		   g2d.draw(lin);

		}
		
	}

	public void update(Graphics g) {

		if (dbImage == null) {
			dbImage = createImage(wWidth, wHeight);
			dbg = dbImage.getGraphics();
		}

		paint(dbg);

		g.drawImage(dbImage, 0, 0, this);
	}
	


	public void setThings(int wHeight, int wWidth, Composite u) {
	
		rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		originalComposite = u;
	
		thingsSet = true;
	}

	
	
}

