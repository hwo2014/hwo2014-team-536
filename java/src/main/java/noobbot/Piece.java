package noobbot;

public class Piece {

	private final double[] length, triangleRatios;
	private double radius, angle;
	private double[] travelTime, speeds;
	private boolean hasSwitch;
	
	public Piece(double[] lengths, double r, double a, boolean hasSwitch){
		this.length=lengths;
		this.travelTime=new double[lengths.length]; // as many traveltimes as there is lanes
		this.speeds=new double[lengths.length]; // as many speeds as there is lanes
		this.radius=r;
		this.angle=a;
		this.hasSwitch=hasSwitch;
		this.triangleRatios=new double[lengths.length];
		
		for(int i=0; i<lengths.length; i++){
			if(a==0){
				this.triangleRatios[i]=1;
			}else{
				double tempRadius=(180*lengths[i])/(Math.PI*a);
				this.triangleRatios[i]=this.length[i]/(Math.sqrt(2*Math.pow(tempRadius,2)-2*Math.pow(tempRadius,2)*Math.cos(Math.toRadians(a))));	
			}
		}
	}
	
	public void setTravelTime(int lane){
		this.speeds[lane]=Global.defaultMaxSpeed;
//		Debug.print("setting speed "+Global.defaultMaxSpeed); 
		this.travelTime[lane]=this.length[lane]/Global.defaultMaxSpeed;
	}
	
	public void setTravelTime(int lane, double speed){
		this.speeds[lane]=speed;
		this.travelTime[lane]=this.length[lane]/speed;
	}
	
	public double getTravelTime(int lane){
		return this.travelTime[lane];
	}
	
	public double getSpeed(int lane){
		return this.speeds[lane];
	}
	
	public double getRadius(){
		return this.radius;
	}
	
	public double getRadius(int lane){
		if(this.angle==0) return this.radius;
		
		return (this.angle>0 ? this.radius-Global.laneDistances[lane] : this.radius+Global.laneDistances[lane]);
	}


	public double getAngle(){
		return this.angle;
	}
	
	public double getLength(int lane){
		return this.length[lane];
	}
	
	public double switchExtraTime(int startLane, int endLane){
		if(!this.hasSwitch) return 0;
		
		double avgLength=(this.length[startLane]+this.length[endLane])/2;

		double laneDist=Math.abs(Global.laneDistances[startLane]-Global.laneDistances[endLane]);

		double switchLength=Math.sqrt(Math.pow(avgLength,2)+Math.pow(laneDist,2));
		
		switchLength*= this.angle==0 ? 0.995 : 1.005; 
		
		double switchingTime = switchLength/this.speeds[startLane];
			
		return switchingTime-this.travelTime[startLane];
	}
	
	
	public double getLength(){
		double avglength=0;
		
		for(int i = 0; i<this.length.length; i++){
			
			avglength+=this.length[i];
			
		}
		
		avglength/=this.length.length;
		
		return avglength;
	}
	
	public double getLength(int startLane, int endLane){
		if(!this.hasSwitch || startLane==endLane) return this.length[endLane];
		
		double avgLength=(this.length[startLane]+this.length[endLane])/2;

		double laneDist=Math.abs(Global.laneDistances[startLane]-Global.laneDistances[endLane]);

		double switchLength=Math.sqrt(Math.pow(avgLength,2)+Math.pow(laneDist,2));
		
		return switchLength;
		
	}
	
	public double triangleRatio(int lane){ return this.triangleRatios[lane]; }
	
	public boolean hasSwitch(){
		return this.hasSwitch;
	}
}
