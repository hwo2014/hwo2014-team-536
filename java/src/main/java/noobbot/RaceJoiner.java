package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;

public class RaceJoiner implements Runnable {

    final Gson gson = new Gson();

    private String name,trackName="",pass;
    private int carCount;
    private PrintWriter writer;
    private int playerID;

    
    public RaceJoiner(String name1, String trackName1, String pass1, int carCount1){
    	this.name=name1;

    	this.trackName=trackName1;
    	this.pass=pass1;
    	this.carCount=carCount1;
    	
    }

	public void RaceJoin() throws UnknownHostException, IOException, NoSuchFieldException, SecurityException, InterruptedException {
		
		if(carCount>1) this.name+=" " + (100+new Random().nextInt(900));
		
	    do{

		    Socket socket=new Socket(Config.host, Config.port);       
		    socket.setSoTimeout(0);
		    socket.setTcpNoDelay(true);
	    
		    final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
	        this.writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
        
        	if(Config.loopAllTracks){
        		this.trackName=lib.randomTrack();
        		lib.hardReset();
        	}
        	
			Debug.print3("Joining race... " + this.trackName);
			
			if(carCount==-1){ // quick race
				send(new join(this.name, Config.botKey));
			}else{
				send(new joinRace(this.name, Config.botKey, trackName, pass, carCount));
			}
			
			Global.players.add(new Player(this.name, false));
	    	this.playerID=Global.players.size()-1;
	        
	        String line;
			while((line = reader.readLine()) != null) {
	
	        	lib.statusreport(Global.players.get(this.playerID));
	        	
	            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
	            
	            if(!msgFromServer.msgType.equals("carPositions")) {
	            	Debug.print("[SERVER]\t" + line);
	            }else{
	            	Debug.print2("[SERVER]\t" + line);
	            }
	            
	            long start_time = System.nanoTime();
	            SendMsg msg =MsgHandler.getSendMsg(msgFromServer, this.name, line);
	           
	            if(msg!=null) send(msg);
	            
	            double time= ( System.nanoTime() - start_time)/1e6;
	            
	            if(time>5) Debug.print3("[LOOP]\tResponsing time:\t" + time +" ms");
//	            if(time>50 && Global.gameTick>0)  	Debug.print3("[WARNING... SERVER]\t" + line);
	
//	            if(Global.dragAndMassFound) lib.mathCSV(this.playerID);
	            
	        }
			
			Debug.print("[SERVER]\t" + line);
			
			socket.close();
			
	    	Debug.print3("[PHYSICS]\tMax. centripetal force:\t" + Global.maxForce);
	    	
	    	Debug.print3("[PHYSICS]\tMax. slip angle:\t" + Global.maxAngle);
	    	
			Debug.print3("[PHYSICS]\tDrag:\t\t" + Global.drag);
			
			Debug.print3("[PHYSICS]\tMass:\t\t" + Global.mass);
			
			Debug.print3("[PHYSICS]\tMax speed:\t" + Global.players.get(playerID).terminalVel());
			
			Debug.print3("[PHYSICS]\tMin avg. speed:\t" + Global.minAvgSpeed);
			
			Debug.print3("[PHYSICS]\tAcceleration:\t" + Global.acceleration);
			
			Debug.print3("[AI]\t\tSpawning time:\t" + Global.spawningTime);
			
			Debug.print3("[AI]\t\tCrash count:\t" + Global.players.get(playerID).crashCount());
			
			TimeUnit.SECONDS.sleep(1);
			
        }while(Config.loopAllTracks);
        
    }
    
    private void send(final SendMsg msg) {
    	Debug.print("[BOT]\t" + this.name +  ":\tSEND\t" + msg.toJson());
    	writer.println(msg.toJson());
    	writer.flush();
    	if(msg.getClass()==Throttle.class) Global.players.get(playerID).setThrottle(Double.parseDouble(msg.msgData().toString()));
    }
    

    class joinRace extends SendMsg {
    	
    	public final botID botId;
        public final String trackName;
        public final String password;
        public final int carCount;

        /*
    	      {"msgType": "joinRace", "data": {
    		  "botId": {
    		    "name": "schumacher",
    		    "key": "UEWJBVNHDS"
    		  },
    		  "trackName": "hockenheimring",
    		  "password": "schumi4ever",
    		  "carCount": 3
    		}}
         */
        joinRace(final String name, final String key, final String trackName, final String pass, final int carCount) {
            this.botId=new botID(name, key);
            this.trackName=trackName;
            this.password=pass;
            this.carCount=carCount;
        }

        @Override
        protected String msgType() {
            return "joinRace";
        }
    }
    
 class join extends SendMsg {
    	
        public final String name;
        public final String key;

        /*
			{"msgType": "join", "data": {
			  "name": "Schumacher",
			  "key": "UEWJBVNHDS"
			}}
         */
        join(final String name, final String key) {
            this.name=name;
            this.key=key;
        }

        @Override
        protected String msgType() {
            return "join";
        }
    }
    
    class botID  {
        public final String name;
        public final String key;
        
        botID(final String name, final String key ){
        	this.name=name;
        	this.key=key;
        }
    }


	@Override
	public void run() {
		try {
			RaceJoin();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
