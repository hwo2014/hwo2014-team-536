package noobbot;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AI {
  
    static int switchedAtPiece=-1;
    static int lastPiece=-1;
    static int fastestLane;

	public static SendMsg Throttler(int ID){
		
//		if(true) return new Throttle(0.666);
		
			if(Global.players.get(ID).finished()) return new Ping();
			
	   		if(Global.players.get(ID).crashed()) return new Throttle(0.666);
	   		
			int pdx=Global.players.get(ID).pieceIdx();

			double speed=Global.players.get(ID).speed();
			int mylane=Global.players.get(ID).getStartLane();
			
			try{
				
			int nextpiece=lib.nextPiece(pdx,1);
			double ang=Global.players.get(ID).predictedAngle(), slipSpeed=Global.players.get(ID).slipSpeedPredicted();
			double pieceang=Global.track.get(ID).getAngle();
			
			boolean nextSwitchOK=switchedAtPiece!=pdx;
			boolean nextHasSwitch=nextSwitchOK ? Global.track.get(nextpiece).hasSwitch() : false;
			int direction=ang<0 ? 1 : -1;
			
			boolean canSwitch = nextHasSwitch ? lib.canSwitch(mylane,direction) : false;
			boolean endOfCorner=Global.players.get(ID).endOfCorner();
			
			if(Math.abs(ang)>=Global.maxAngle*0.99){

				Debug.print("[AI]\tToo high predicted angle! " + ang);
				
				if(canSwitch){
					if((Global.track.get(pdx).getLength(mylane)-Global.players.get(ID).pieceDist())<(Global.maxSlipLength-Math.abs(Global.players.get(ID).slipLength()+slipSpeed))){
						Debug.print("[AI]\tSwitching lane");
						switchedAtPiece=pdx;
						switchingAt=nextpiece;
						return new switchLane(direction>0 ? "Right" : "Left"); 
					}
				}
				
				double nextang=Global.track.get(nextpiece).getAngle();
				
//				double slipV=(speed+Math.abs(Global.players.get(ID).acceleration())+Math.abs(slipSpeed*2)*Global.slipRatio)*Global.track.get(pdx).triangleRatio(mylane);
				
				if(((((nextang== 0 || canSwitch || !lib.isSameWay(ang, nextang)) || !lib.isSameWay(nextang, pieceang))|| pieceang==0)) && endOfCorner) {

					Debug.print("[AI]\tTrying to handle the angle with full throttle");
					
					targetSpeed=Global.defaultMaxSpeed;

					return new Throttle(1);	
				
				}		  
			}

			
			if(endOfCorner) laneDistances = DistToOpponent(pdx, ID, speed);
			
			if(shouldTurbo(pdx, ID, Global.players.get(ID).safeEnd()) && (targetSpeed>speed || ramming || lastStraight || targetSpeed==Global.defaultMaxSpeed)){
				
				if(Global.players.get(ID).getTurbo()[0]==1){
					Global.players.get(ID).useTurbo();
					return new Turbo("Live free and burn.");
				}
			}
			
			if(tryTeleport(pdx, ID)){
				Debug.print("[AI]\tTrying to teleport on switch");
				return new Throttle(1.0);
			}else if(speed<2){
				Debug.print("[AI]\tFull power! Speed<2");
				return new Throttle(1);				
			}
			
	
			if(lastPiece!=pdx) fastestLane=switchToFastestLane(mylane, ID);
			lastPiece=pdx;
			
	
			if(!ramming && switchedAtPiece != pdx && nextHasSwitch){
	
				int[] switchStuff = switchToFreeLane(mylane, pdx);
				int switchLane=switchStuff[0];
				int opponentNear=switchStuff[1];
				switchedAtPiece=-1;
				
				if(opponentNear != 0){
					if(switchLane>-1){

						int lane=(Global.players.get(ID).getEndLane());
		
						if(mylane==lane){
							Debug.print("[AI]\tOvertaking");
							switchedAtPiece=pdx;
							return new switchLane(switchLane==1 ? "Right" : "Left");
						}
					}
				}else if(!lastStraight){

					// TODO: Fastest lane?
						
					if(fastestLane>-1 ){
						Debug.print("[AI]\tSwitching to shorter lane");
						switchedAtPiece=pdx;
						return new switchLane(fastestLane==1 ? "Right" : "Left"); 
					}
					
				}
			}
			}catch(Exception e){
				e.printStackTrace();
			}
			

			if(lastStraight || ramming || !Global.speedsFound ){
				Debug.print("[AI]\tFull power! (" + (lastStraight ? "last straight" : "") + (ramming ? "ramming" : "") + ")");
				return new Throttle(1);
			}
			
			
			targetSpeed = findMaxSpeed(speed, ID, false);
			
			Debug.print("[AI]\tTarget speed:\t\t" + targetSpeed + " - now: " + speed);
			
			double tempSpeed=targetSpeed;
			
			double difference=Math.abs(tempSpeed-speed);
			
			if(difference<0.1 && targetSpeed!=Global.defaultMaxSpeed){
				if(!Global.players.get(ID).turboLeft()){
					tempSpeed*=speedToThrottle(ID);
					return new Throttle(tempSpeed >= 1 ? 1 : tempSpeed);
				}
			}

			
			return new Throttle(tempSpeed > speed ? 1 : 0);  

		
	}
	
	static double speedToThrottle(int ID){
		return 1/Global.players.get(ID).terminalVel();
	}
	
	static double targetSpeed=99;
	
	public static double minSpeed=6.66;

	 public static double findMaxSpeed(double speed, int id, boolean parseTrack){
		 // id is used for lane when parsin track :D
		 	
		 	int inAccuracy=parseTrack ? 6 : 2;
		 	
		 	double brakeDist, maxSpeed, holdOn=0, distance=0, tempdist=0, inPiece;
		 	int piece, endLane,startLane;
		 	
		 	if(!parseTrack){
		    	brakeDist=lib.brakingDistance((speed>minSpeed ? minSpeed : speed*0.666), id);
		    	piece = Global.players.get(id).pieceIdx();
		    	
		    	Debug.print("[AI]\tBraking distance to speed of " + (speed>minSpeed ? minSpeed : speed*0.666) + ":\t" + brakeDist);
		    	
		    	if(brakeDist>666) brakeDist=666;
	
		    	maxSpeed=Global.players.get(id).terminalVel();
		    	inPiece = Global.players.get(id).pieceDist();
		    	
		    	endLane=bestlane;
		    	startLane=Global.players.get(id).getStartLane();
		 	}else{ // finding fastest pieces
		 		brakeDist=Global.trackLength;
		 		piece=0;
		 		startLane=id;
		 		endLane=id;
		 		inPiece=0;
		 		maxSpeed=0;
		 		// TODO:
		 	}
//	    	Debug.print("piece: " + piece + " tick: " + Global.gameTick + " lap: " + Global.players.get(id).getLap());
	    	
	    	int nextpiece=piece;
	    	List<double[]> corner = new ArrayList<double[]>();

	    	// find next corner
	    	while(distance<brakeDist){
	    	
		    	Debug.print2("bestlane: " + bestlane + " , startLane: " + startLane);
	    		if(Global.track.get(nextpiece).getAngle()!=0){
	    			Debug.print2("Corner after " + lib.distToPiece(piece, nextpiece) + " pieces: ");
	    			double temp[]=lib.getCorner(nextpiece, startLane, endLane, nextpiece==piece ? inPiece : 0, Config.deepDebug, true);
					corner.add (temp);  //length, ang, r, inpiece
//					Debug.print("temp[0]" + temp[0] + " & temp[3]" + temp[3]);
					double temp2=temp[0]-temp[3];
					distance+=temp2>0 ? temp2 : 1;
					int tempnextpiece=(int) temp[5];
//					Debug.print("nextpiece " + nextpiece);
					
					if(startLane!=endLane && temp[6]!=0){
						if(lib.isBetweenPieces(switchingAt, nextpiece, tempnextpiece)) {
							holdOn = lib.isSameWay(temp[1],Global.laneDistances[startLane]) ? -Global.mass/15 : Global.mass/15; 
							startLane=endLane;	
						}else{
							corner.get(corner.size()-1)[6]=0;
						}
					}
					
					nextpiece=tempnextpiece >= nextpiece ? tempnextpiece : lib.nextPiece(nextpiece,1);
					
	    		}else{
	    			if(distance==0) inPiece=0; // first piece and it seems we arent in a corner atm so we reset this because we would use it later to skip some of the corner calculations
	    			double dist=Global.track.get(nextpiece).getLength(startLane, endLane);	
	    			distance+=dist;
	    			if(corner.size()==0) tempdist+=dist;
	    			
	    			if(startLane!=endLane){
	    				if(nextpiece==switchingAt){
//	    					holdOn = Global.mass/10;
	    					startLane=endLane; 
	    				}
	    			}
	    			 
	    			if(parseTrack){
	    				Debug.print("STRAIGHT " + nextpiece);
	    				Global.track.get(nextpiece).setTravelTime(startLane); // straight -> max speed -> min. travelling time
	    			}
	    		}
	    			    		
//	    		Debug.print("distance" + distance + " / " + brakeDist);
	    		
	    		nextpiece = lib.nextPiece(nextpiece, 1);
	    		
	    	}
	    	
	    	if(!parseTrack){
		    	if(corner.size()==0){
		    		Debug.print2("[AI]\tNo corners near");
		    		return maxSpeed; // full speed
		    	}else{
		    		Debug.print2("[AI]\t" + corner.size() + " corners near");
		    	}
	    	}
	    	
	    	double inCornerAngle = 0, angleFromCenter, tempMax=speed;
	    	
	    	double carMass=Global.mass;
	    
	    	double brakeDist2=0;
	    	
	    	distance=tempdist;
	    	
	    	double avgSpeed=0, sum=0;

	    	double maxF;
	    		
	    	double slipF=0;
	    	if(!parseTrack){
	    		int dist=lib.distToPiece(piece, (int) corner.get(0)[4]);
	    		if(dist<3 && dist>=0){
	    			slipF=Global.players.get(id).slipForce()*Global.drag*(Global.mass/2);
	    			Debug.print("[AI]\tSlip force adjuster:\t" + slipF);
	    		}
	    	}
	    	
	    	double prevAng=0;
	    	
	    	for(int i=0; i<corner.size(); i++){
				double pieceRadius=corner.get(i)[2];
		    	Debug.print2("[AI]\tCorner " + i  );
		    	double cornerAngle=corner.get(i)[1];
		    	prevAng=0;
		    	
		    	if(i==0){
		    		inPiece=corner.get(i)[3];
		    	}else{
		    		inPiece=0;
		    		int start=(int)corner.get(i-1)[5],end=(int)corner.get(i)[4];
		    		if(lib.distToPiece(start,end)==0){
		    			if(lib.isSameWay(cornerAngle,corner.get(i-1)[1])){
		    				prevAng=Math.abs(corner.get(i-1)[1]);
		    			}
		    		}else{
		    			distance+=lib.distToPiece2(start,end, true); // a straight between these corners (we only have corners in the list) so let's add the distance to braking distance calculations
		    		}
		    	}

		    	if(cornerAngle<0){
		    		maxF=(corner.get(i)[6]!=0? -holdOn: 0 ) +Global.maxForce+(i>0  ? 0 : slipF > 0 ? slipF/3 : slipF);
		    		
		    	}else{
		    		maxF=(corner.get(i)[6]!=0? -holdOn: 0 )+Global.maxForce-(i>0  ? 0 : slipF < 0 ? slipF/3: slipF);   		
		    	}
		    	
		    	Debug.print2("maxF: " + maxF + " / Global.maxForce" + Global.maxForce);
		    	
		    	double stopAtAngle=Math.abs(cornerAngle)+prevAng>180 ? Math.abs(cornerAngle)+prevAng-Global.maxAngle : 180-Global.maxAngle;
		    			    	
				while(inPiece<corner.get(i)[0] && (inCornerAngle<stopAtAngle || parseTrack)){ 
			
					inPiece+=inAccuracy;
					
					inCornerAngle=prevAng+360*(inPiece/(2*Math.PI*pieceRadius));
					
					angleFromCenter=Math.abs(90 - (inCornerAngle > 90 ? 90 : inCornerAngle));
					
					double temp=maxF-  carMass*Math.cos(Math.toRadians(angleFromCenter));
					
					if(temp<=0){
						tempMax=1;
					   	Debug.print("[ERROR]\tToo low maxForce " + Global.maxForce);
					   	Global.maxForce*=1.01;
					}else if((inCornerAngle<stopAtAngle || parseTrack)){
						tempMax=Math.sqrt(temp* pieceRadius/carMass);
					}
	
				   	Debug.print2("inPiece " + inPiece 
							   				+ "\ntempMax " + tempMax 
							   				+ "\ninCornerAngle " + inCornerAngle
							   				+"\npieceRadius " + pieceRadius +"\n" );
			    	
					if(tempMax<maxSpeed ){
						if(speed>tempMax){
							brakeDist2=lib.brakingDistance(tempMax, id);
							Debug.print2("brakeDist to corner " +i + " : " + brakeDist2 + " speed: " + tempMax );
						}
						maxSpeed = tempMax;

					}

					if(parseTrack){
						sum++;
						avgSpeed+=tempMax;
					}
					
					if(Double.isNaN(tempMax)){
						Debug.print("tempMax: NaN");
						Debug.print("maxF: " + maxF);
						Debug.print("carMass: " + carMass);
						tempMax=2;
					}
				}
				
				distance+=corner.get(i)[0];
				
				if(parseTrack){
					lib.setTravelTimes(startLane, corner.get(i)[4], corner.get(i)[5], avgSpeed/sum);
					sum=0;
					avgSpeed=0;
				}else if(brakeDist2>distance){
					Debug.print2("BREAK: brakeDist2>distance " + brakeDist2 + " > " + distance);
					maxSpeed=0;
					break;
				}else{
					inAccuracy++;
				}
				
				
	    	}	
	    	
	    	if(maxSpeed<minSpeed && maxSpeed>0){
	    		minSpeed=maxSpeed*0.9;
	    		Debug.print2("new minSpeed: " + minSpeed);
	    	}
	        
			return maxSpeed;
	    }
	

	static boolean lastStraight=false;
	static boolean ramming=false;
	
	static int straightAmount=0, straightPieces=0;
	

	static boolean shouldTurbo(int piece, int id, boolean halfway){
		
		double[] turbo=Global.players.get(id).getTurbo();
		
		if(turbo[0]==0 && !ramming && !Global.players.get(id).turboLeft())	return false;
		
		int endlane=Global.players.get(id).getEndLane();
				
		int i;
		lastStraight=false;

		straightAmount=0; straightPieces=0;
			
		int next = halfway ? lib.nextPiece(piece,1) : piece;
		for(i=0; i<Global.track.size(); i++){
					
			if(Math.abs(Global.track.get(next).getAngle()) == 0){
				straightAmount+=Global.track.get(next).getLength(endlane);
				straightPieces++;
			}else{
				break;
			}
			
			next=lib.nextPiece(next,1);
			
		}
		
			
		if(Global.players.get(id).stupid()==false){
			Debug.print("[AI]\tStraight amount:\t" + straightAmount);
	
			if(laneDistances[endlane]==2 || laneDistances[endlane]==1){
				double pieceang=Global.track.get(lib.nextPiece(piece, straightPieces+1)).getAngle();
				
				if(straightPieces>laneDistances[endlane] && straightPieces<4 && pieceang!=0){

						Debug.print3("[AI]\tRamming possibility at lane " + endlane + " in " + laneDistances[endlane] + " pieces ... speed: " + laneSpeeds[endlane]);
						ramming=true;
						return true;

				}
				
			}else{
//				Debug.print("enemy not near: " + laneDistances[endlane]);
				ramming=false;
			}
		
		}

		if(straightPieces>0) lastStraight=( Global.players.get(id).getLap()==Global.laps && piece>Global.track.size()*0.66 && next<Global.track.size()*0.33);

		if(piece==Global.straight[0] || piece==Global.straight[1] || lastStraight){
			Debug.print("[AI]\tLast straight or longest straight on race/track ... lastStraight: " + lastStraight);
			return true; 
		}else{
//			Debug.print("[AI]\tpiece " + piece + " laststraight: " + lastStraight + " Global.straight[0] " + Global.straight[0]);
			return false;			
		}
		
	}
	

	
	static boolean tryTeleport(int mypiece, int myID){
		
		int startlane, pieceidx;
		double mydist;
		double myspeed;
		
		int nextpiece=lib.nextPiece(mypiece,1);

		if(Global.track.get(nextpiece).hasSwitch()==false) return false;
		
		if(Math.abs(Global.track.get(lib.nextPiece(nextpiece,1)).getAngle())>0) return false;
		
		int mylane=Global.players.get(myID).getStartLane();

		if(mylane!=Global.players.get(myID).getEndLane()) return false; // i'm switching too
		
		myspeed=Global.players.get(myID).speed();
		
		mydist=Global.players.get(myID).pieceDist();
		
		for(int i=0; i<Global.players.size(); i++){
			
			pieceidx=Global.players.get(i).pieceIdx();
			if(pieceidx==mypiece){
				startlane=Global.players.get(i).getStartLane();
				
				if(Math.abs(startlane-mylane)==1){
					if(Global.players.get(i).speed()>myspeed) return true;
					if(Global.players.get(i).pieceDist()>mydist) return true;
				}
				
			}
	
		}
		
		return false;
		
	}

	public static int[] DistToOpponent(int pieceIndex, int myID, double speed){
		
		double[] laneSpeed = new double[Global.laneDistances.length];
		int[] lane = new int[Global.laneDistances.length];
		for(int i=0; i<lane.length; i++){
			lane[i]=999;
			laneSpeed[i]=999;
		}
		
		boolean skip;
		
		if(Global.players.size()==1) return lane;
		
		int endlane=0, dist;
		int pieceidx;
		double myInPiece=Global.players.get(myID).pieceDist();
		
		for(int i=0; i<Global.players.size(); i++){
				
			skip=false;
			
			if(i != myID &&  Global.players.get(i).finished()==false){
				
				endlane=Global.players.get(i).getEndLane();
				
				if(Global.players.get(i).crashed()){
					int spawningIn=Global.players.get(i).spawningIn();
					if(spawningIn>0 && spawningIn>laneTimes[endlane]){
						Debug.print("[AI]\tA player spawning in " + spawningIn + " at lane "  + endlane + " but laneTime is: " + laneTimes[endlane]);
						skip=true;
						
					}else{
						Debug.print3("[AI]\tA player spawning soon at lane " + endlane);
					}
				}
				
				if(!skip){
					pieceidx=Global.players.get(i).pieceIdx();
					dist=lib.distToPiece(pieceIndex, pieceidx);
					
					Debug.print("[AI]\tPieces to " + Global.players.get(i).name() + ":\t" + dist + "\t(lane " + Global.players.get(i).getEndLane() +")");
					
					if(dist<lane[endlane]){
						if(pieceidx==pieceIndex){
							if(Global.players.get(i).pieceDist()>myInPiece){
								double laneLength=Global.track.get(pieceidx).getLength(endlane);
								double mylaneLength=Global.track.get(pieceidx).getLength(Global.players.get(myID).getEndLane());
	
								laneSpeed[endlane]=Global.players.get(i).speed()*(laneLength/mylaneLength);
							}else{
								dist=999; // dont give free way to the one behind me by switching lane
							}
							
						}else{
							laneSpeed[endlane]=Global.players.get(i).speed();
						}
						
						if(dist!=999) lane[endlane]=dist;
					}
				}
			}
		}

		double targetSpeedTemp=targetSpeed>Global.defaultMaxSpeed ? Global.defaultMaxSpeed : targetSpeed;
		for(int i=0; i<lane.length; i++){
			if(lane[i]>targetSpeedTemp*0.333 || (laneSpeed[i]>=targetSpeedTemp  && lane[i]>0)) lane[i]= 999;
//			Debug.print(i + ": " + lane[i]);
		}
				
		laneSpeeds=laneSpeed;
		return lane;
		
		
	}
	
	static int piecesToNextSwitch(int piece, int skip){
		piece=lib.nextPiece(piece ,skip);
		
		for(int i=0; i<Global.track.size(); i++){
			
			if(Global.track.get(piece).hasSwitch()){
				return i;			
			}
			
			piece=lib.nextPiece(piece ,1);
		}

		return 0;
	}
	
	static int switchingAt=-1;

	static int bestlane=0;
	
	static double[] laneTimes;
	static int switchToFastestLane(int mylane, int id){
		
		 if(laneTimes==null) laneTimes = new double[Global.laneLengths.length];
		 
		 int mypiece=Global.players.get(id).pieceIdx();
		 boolean nextHasSwitch=Global.track.get(lib.nextPiece(mypiece,1)).hasSwitch()  ;
		 
		 Debug.print2("mypiece= " + mypiece);
		 
		 if(!nextHasSwitch && switchedAtPiece!=mypiece){
			 
			 mypiece=lib.nextPiece(mypiece, piecesToNextSwitch(mypiece,0));
			 Debug.print2("mypiece2= " + mypiece);
		 }
		 
		 int forwardAmount = piecesToNextSwitch(mypiece,  2);
		 
		 if(forwardAmount<=0 || lib.distToPiece(mypiece, switchingAt)>Global.track.size()/2) return -1;
		 
		 Debug.print("[AI]\tPieces to next switch: " + forwardAmount);
		 
		 double shortestWay=999999999;
		 int tempbestlane=mylane;
		 
		 for(int i=0; i<Global.laneLengths.length; i++){

			 int piece=mypiece;
			 
			 laneTimes[i] = 0;
			 
			 for(int z=0; z<forwardAmount; z++){
				 
				 laneTimes[i] += Global.track.get(piece).getTravelTime(i);
				 piece = lib.nextPiece(piece, 1);
				 
			 }

			 if(i != mylane){
				 laneTimes[i]+= Global.track.get(lib.nextPiece(mypiece, 1)).switchExtraTime(mylane, i);
			 }
			 
			 Debug.print("[AI]\tLane " + i + " travel time before next switch: " + laneTimes[i]);
			 if(laneTimes[i]<shortestWay || (laneTimes[i] == shortestWay && i == mylane)){
				 shortestWay=laneTimes[i];
				 tempbestlane=i;
			 }
			 
		 }
		 
		 if(tempbestlane != mylane ){
			 switchingAt=lib.nextPiece(mypiece,forwardAmount+2);
			 int direction =tempbestlane<mylane ? -1: 1;
			 bestlane=mylane + direction;
			 if(!nextHasSwitch) return -1;
			 return (tempbestlane<mylane ? 0: 1);
		 }else{
			 bestlane=mylane;
			 switchingAt=-1;
			 return -1;
		 }
		
	}

	static String switchNow(int mylane){
		
		int newlane;
		
		if(mylane==0){
			newlane= 1;

		}else if(mylane==Global.laneDistances.length-1){

			newlane= mylane-1;
	
		}else{

			newlane= new Random().nextInt(3)+mylane-1;

		}
		
		return (newlane>mylane ? "Right" : "Left");
	}
	
	 static int[] laneDistances;
	 static double[] laneSpeeds=new double[2];
	 
	 static int[] switchToFreeLane( int mylane, int piece){
			
			int[] response=new int[2];

			int bestlaneTemp=mylane;
			boolean opponentnear=false;
			
			if(mylane==0){
				if(laneDistances[0]==999){
					bestlaneTemp=mylane;
//						Debug.print("1");
				}else{
//						Debug.print("2");
					bestlaneTemp=(laneDistances[0]>laneDistances[1] ? 0 : 1);
				}
			}else if(mylane==Global.laneDistances.length-1){
				if(laneDistances[mylane]==999){
//						Debug.print("3");
					bestlaneTemp=mylane;
				}else{
//						Debug.print("4");
					bestlaneTemp=(laneDistances[mylane]>laneDistances[Global.laneDistances.length-2] ? mylane : Global.laneDistances.length-2);
				}
			}else{
				
				if(laneDistances[mylane]==999){
//						Debug.print("5");
					bestlaneTemp=mylane;
				}else{
//						Debug.print("6");
					bestlaneTemp=(laneDistances[mylane-1]>laneDistances[mylane+1] ? mylane-1 : mylane+1);
					if(laneDistances[bestlaneTemp]<laneDistances[mylane]) bestlaneTemp=mylane;
				}
			}
			
			for(int i=0; i<laneDistances.length; i++){
				
				if(laneDistances[i]!=999){
					opponentnear=true;
				}

			}
			
			
//			Debug.print("bestlane:\t" +bestlane);
			if(bestlaneTemp != mylane){
				response[0]=(bestlaneTemp>mylane ? 1 : 0); // 1 right, 0 left
				response[1]=(opponentnear ? 1 : 0);
				if(opponentnear){
					switchingAt=lib.nextPiece(piece, 1);
					bestlane=bestlaneTemp;
				}
			}else{
				response[0]=-1; // dont switch
				response[1]=(opponentnear ? 1 : 0);
			}

			return response;
		}
	
}
