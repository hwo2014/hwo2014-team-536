package noobbot;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Debug {

	
	static String timeStamp = new SimpleDateFormat("MMdd_HHmmss").format(Calendar.getInstance().getTime());
	
	public static void print(String text){
		if(Config.dbg) System.out.println(Global.gameTick + ": " + text);
		if(Config.logToFile) lib.writeLog(text, "logs/" + timeStamp + ".txt",true);
	}
	
	public static void print2(String text){
		if(Config.deepDebug ){
			if(Config.dbg2) System.out.println(Global.gameTick + ": " + text);
			if(Config.logToFile) lib.writeLog(text, "logs/" + timeStamp + ".txt",true);
		}
	}
	
	public static void print3(String text){
			System.out.println(Global.gameTick + ": " + text);
			if(Config.logToFile) lib.writeLog(text, "logs/" + timeStamp + ".txt",true);
	}

	public static void printCars(){
		
		for(int i=0; i<Global.cars.size(); i++){
			print("Car " + i);

				print("\tOwner:\t" + Global.cars.get(i).owner()
				+ "\n\tLength:\t\t" + Global.cars.get(i).length()
				+ "\n\tWidth:\t\t" + Global.cars.get(i).width()
				+"\n\tGuide flag position:\t" + Global.cars.get(i).guideFlagPosition() +"\n");
		}
		
	}
	

	public static void printTrack(){
		
		for(int i=0; i<Global.track.size(); i++){
			print("Piece " + i);
			
			boolean hasSwitch=Global.track.get(i).hasSwitch();
			
			for(int x=0; x<Global.laneDistances.length; x++){
				print("\tLength " + x + ":\t\t\t" + Global.track.get(i).getLength(x));
				print("\tTravelling time " + x + ":\t" + Global.track.get(i).getTravelTime(x));
				print("\tTriangle ratio " + x + ":\t" + Global.track.get(i).triangleRatio(x));
				print("\tSpeed " + x + ":\t" + Global.track.get(i).getSpeed(x));
						
				if(hasSwitch){
					if(lib.canSwitch(x,1))print( "\tSwitching extra time " + x + " -> " + (x+1) + ":\t" + Global.track.get(i).switchExtraTime(x,x+1) + "\n\tSwitching length:\t" + Global.track.get(i).getLength(x,x+1));
					if(lib.canSwitch(x,-1)) print( "\tSwitching extra time " + x + " -> " + (x-1) + ":\t" + Global.track.get(i).switchExtraTime(x,x-1) + "\n\tSwitching length:\t" + Global.track.get(i).getLength(x,x-1));
				}
			}

				print("\tAngle:\t" + Global.track.get(i).getAngle()
				+ "\n\tRadius:\t" + Global.track.get(i).getRadius()
				+ "\n\tSwitch:\t" + hasSwitch +"\n");
		}
		
		for(int x=0; x<Global.laneLengths.length; x++){
			print("Lane "+  x + " length " + Global.laneLengths[x]);

		}
		
		print("\nLongest lane length: " + Global.trackLength + "\n");
		
	}
}
