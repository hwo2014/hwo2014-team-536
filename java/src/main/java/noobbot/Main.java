package noobbot;

import java.io.IOException;
import com.google.gson.Gson;

public class Main {
    public static void main(String... args) throws IOException, InterruptedException {
		
    	if(Config.vis){
    		Thread visualization = new Thread(new visualization.Painter());
    		visualization.start(); 
    	}

    	try{
	        Config.host = args[0];
	        Config.port = Integer.parseInt(args[1]);
	        Config.botName = args[2];
	        Config.botKey = args[3];

    	}catch(Exception e){
    		e.printStackTrace();
    		Debug.print("ERROR: Invalid parameters!!!");
    	}
    	
        String mapname="";
        String pass="foo";
        
        int botAmount=-1;
  	
        try{
        	        	
        	botAmount=Integer.parseInt(args[5]);
        	mapname=args[4];
        	Debug.print(botAmount + " bots");
        	pass=args[6];
        	
	        Config.loopAllTracks = Integer.parseInt(args[7]) == 1 ? true : false;
        }catch(Exception e){   
//        	e.printStackTrace();
        	Debug.print2("Some optional parameters not given");
        } 
        
    	Thread join = new Thread(new RaceJoiner(Config.botName ,  mapname,pass,botAmount));
    	join.start();

    }

}

abstract class SendMsg {
    public String toJson() {
    	try{
    		return new Gson().toJson(new MsgWrapper(this));
    	}catch(Exception e){
    		e.printStackTrace();
    		Debug.print("[MSG]\tInvalid msg:\t" +  new Gson().toJson(new MsgWrapper(this)).toString());
    	}
    	
    	return new Ping().toJson();
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;
    public int gameTick;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick=Global.gameTick;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}



class Join extends SendMsg {
    public final String name;
    public final String key;
	/*
	 * {"msgType": "join", "data": {
	  	"name": "Schumacher",
	  	"key": "UEWJBVNHDS"
		}}
	 */
    
    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}


class Turbo extends SendMsg {
    private String message;

    public Turbo(String message) {
        this.message = message;
    }
    
    @Override
    protected Object msgData() {
        return message;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}

class switchLane extends SendMsg {
    private String direction;

    public switchLane(String direction) {
        this.direction = direction;
    }
    
    @Override
    protected Object msgData() {
        return direction;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}