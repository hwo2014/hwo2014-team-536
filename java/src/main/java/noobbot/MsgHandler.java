package noobbot;

import java.util.ArrayList;
import java.util.Iterator;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class MsgHandler {
	static Gson gson = new Gson();
	
	public static SendMsg getSendMsg(MsgWrapper msgFromServer, String player, String line){

		try{
	        if (msgFromServer.msgType.equals("carPositions")) {        
	        	return(parseCarPosition(line, player));

	    	} else if (msgFromServer.msgType.equals("tournamentEnd")) {
	            System.out.println("Tournament ended");    		
	    	} else if (msgFromServer.msgType.equals("join")) {
	            System.out.println("Joined");
	    	} else if (msgFromServer.msgType.equals("createRace")) {
	            System.out.println("Race created");
	        } else if (msgFromServer.msgType.equals("gameInit")) {
	        	
	        		lib.reset();
        		
	        		Global.track = new ArrayList<Piece>();
		        	parseGameInit(line); 	

		        	Debug.printCars();
	
		        	System.out.println("Race init");
		        	
	        } else if (msgFromServer.msgType.equals("crash")) {
	        	parseCrash( line);
	        } else if (msgFromServer.msgType.equals("dnf")) {
	        	parseDnf( line);	        	
	        } else if (msgFromServer.msgType.equals("spawn")) {
	        	parseSpawn( line);	        	
	        } else if (msgFromServer.msgType.equals("turboAvailable")) {
	            parseTurbo(line);
	        } else if (msgFromServer.msgType.equals("lapFinished")) {
	            parseLapFinish(line);	            
	        } else if (msgFromServer.msgType.equals("gameEnd")) {
	            System.out.println("Race end");
	        } else if (msgFromServer.msgType.equals("gameStart")) {
	        	Global.gameTick=0;
	            System.out.println("Race start");
	        } else if (msgFromServer.msgType.equals("finished")) {
	         	parseFinish(line);
	        } 
		}catch(Exception e){
			e.printStackTrace();
			Debug.print3("MsgHandler error: " + e);
		}
		
//		Debug.print3("[MSG]\t" +msgFromServer.msgType + ": " + line);
		if (msgFromServer.msgType.equals("carPositions")) return new Throttle(1);
		if (msgFromServer.msgType.equals("gameStart")) return new Throttle(1);
//		if(line.contains("gameTick")) return new Ping();
		return null;
	}
	
	static void parseTurbo(String line){
		JsonObject json = null;
		
		json= (JsonObject) new JsonParser().parse(line);
		json=(JsonObject) json.get("data");	
		Double duration =Double.parseDouble(json.get("turboDurationMilliseconds").toString());
		Double factor = Double.parseDouble(json.get("turboFactor").toString());
		Debug.print3("[MSG]\tTurbo available");

		for(int i=0; i<Global.players.size(); i++){
			if(Global.players.get(i).crashed()==false) Global.players.get(i).setTurbo(true, factor,  (int)Math.round(duration/(50/3)));
		}

	}
	
	static void parseLapFinish(String line){
		JsonObject json = (JsonObject) new JsonParser().parse(line);
		 json=(JsonObject) json.get("data");
		 json=(JsonObject) json.get("car");
		String name = json.get("name").toString();

		name=name.substring(1,name.length()-1);

		Debug.print3("[MSG]\t" + name + " finished lap at tick " + Global.gameTick);
		
		String tempname;
		for(int i=0; i<Global.players.size(); i++){
							
				tempname=Global.players.get(i).name();
				
				if(tempname.equals(name)){
					Global.players.get(i).addLap(Global.gameTick);
				
					return;
				}

		}
		
		Debug.print3("Player " + name + " not found from lists!");
	}
	
	static void parseFinish(String line){
		JsonObject json = (JsonObject) new JsonParser().parse(line);
		 json=(JsonObject) json.get("data");	
		String name = json.get("name").toString();

		name=name.substring(1,name.length()-1);

		Debug.print3("[MSG]\t" + name + " finished race");
		
		String tempname;
		for(int i=0; i<Global.players.size(); i++){
							
				tempname=Global.players.get(i).name();
				
				if(tempname.equals(name)){
					Global.players.get(i).finish();
					return;
				}

		}
		

	}
	
	
	static void parseSpawn(String line){
		JsonObject json = (JsonObject) new JsonParser().parse(line);
		 json=(JsonObject) json.get("data");	
		String name = json.get("name").toString();

		name=name.substring(1,name.length()-1);

		Debug.print3("[MSG]\t" + name + " spawned at tick " + Global.gameTick);
		
		String tempname;
		for(int i=0; i<Global.players.size(); i++){
							
				tempname=Global.players.get(i).name();
				
				if(tempname.equals(name)){
					Global.players.get(i).spawn();
					return;
				}

		}
		
		Debug.print3("Player " + name + " not found from lists!");
	}
	
	
	static void parseDnf(String line){
		JsonObject json = (JsonObject) new JsonParser().parse(line);
		 json=(JsonObject) json.get("data");
		String reason = json.get("reason").toString();
		 json=(JsonObject) json.get("car");
		String name = json.get("name").toString();

		name=name.substring(1,name.length()-1);

		Debug.print3("[MSG]\t" + name + " DNF: " + reason);
		
		String tempname;
		for(int i=0; i<Global.players.size(); i++){
							
				tempname=Global.players.get(i).name();
				
				if(tempname.equals(name)){
					Global.players.get(i).crash(true);
				
					return;
				}

		}
		
		Debug.print3("Player " + name + " not found from lists!");
	}
	
	static void parseCrash(String line){
		JsonObject json = (JsonObject) new JsonParser().parse(line);
		 json=(JsonObject) json.get("data");	
		String name = json.get("name").toString();

		name=name.substring(1,name.length()-1);

		Debug.print3("[MSG]\t" + name + " crashed at tick " + Global.gameTick);
		
		String tempname;
		for(int i=0; i<Global.players.size(); i++){
							
				tempname=Global.players.get(i).name();
				
				
				if(tempname.equals(name)){

					Global.players.get(i).crash(false);
					
					return;
				
				}
				
		}
		
		Debug.print3("Player " + name + " not found from lists!");
		
	}
	
	static SendMsg parseCarPosition(String line, String myName){
		
//		boolean first=false;
		JsonObject json = (JsonObject) new JsonParser().parse(line);
//		Debug.print(json.toString());
		int tick=0;
		try{
			tick=Integer.parseInt(json.get("gameTick").toString());
			if(Global.gameTick<tick || tick<2){
				Global.gameTick=tick; // multiple bots...
	
			}
		}catch(Exception e){
//			first=true;
		}
		
		JsonElement data=json.get("data");
//		Debug.print(data.toString());
		JsonArray array= data.getAsJsonArray();
		@SuppressWarnings("rawtypes")
		Iterator iterator = array.iterator();
		
		/* Player.java:
				 
			private String name;
			private int ID, pieceIndex;
			private double inPieceDistance, angle;
			private boolean isGuest;
		 */
		
		int me=-1;
//		Debug.print("myname " + myName);
		while(iterator.hasNext()){
			String name="";
			int ID=-1, pieceIndex=-1;
			double inPieceDistance=-1, angle=360;
			
			JsonObject temp=(JsonObject) iterator.next();
//			Debug.print("i " + temp.toString());
			
			JsonObject temp2=(JsonObject) temp.get("id");
//			Debug.print("i" + temp2.toString());
			
			name=temp2.get("name").toString();
			name=name.substring(1,name.length()-1);
//			Debug.print("name " + name);
			
			ID=lib.listIDbyName(name);
			if(name.equals(myName)) me=ID;
						
			angle=Double.parseDouble(temp.get("angle").toString());
			
			temp2=(JsonObject) temp.get("piecePosition");
			
			pieceIndex = Integer.parseInt(temp2.get("pieceIndex").toString());
			inPieceDistance = Double.parseDouble(temp2.get("inPieceDistance").toString());
			
			int endLane=Integer.parseInt(((JsonObject) temp2.get("lane")).get("endLaneIndex").toString());
			
			int startLane=Integer.parseInt(((JsonObject) temp2.get("lane")).get("startLaneIndex").toString());
			
			if(ID>-1){
				if(tick>Global.players.get(ID).getLastTick()) {
						
					if(inPieceDistance>=0){
						Global.players.get(ID).setSpeed(inPieceDistance, pieceIndex, tick);
						

						Global.players.get(ID).setPieceDist(inPieceDistance);
					}
					
					Global.players.get(ID).setStartLane(startLane);
					
					Global.players.get(ID).setEndLane(endLane);
					

					
					Global.players.get(ID).setPieceIndex(pieceIndex);
					
					Global.players.get(ID).maths(angle, tick);
					
					Global.players.get(ID).setTick(tick);
					
				}else if(tick==0){
					Debug.print3("Setting inPieceDistance " + inPieceDistance + " to " + Global.players.get(ID).name());
					Global.players.get(ID).setPieceDist(inPieceDistance);
					Debug.print3("Setting pieceindex " + pieceIndex + " to " + Global.players.get(ID).name());
					Global.players.get(ID).setPieceIndex(pieceIndex);
				}
			}else{
				Debug.print3("New player: " + name);
				Global.players.add(new Player(name,pieceIndex,inPieceDistance,angle,true));
			}
			
		}
		
		if(me>-1){
//			if(first) return null;
			return AI.Throttler(me);
		}else{
			Debug.print3("ERROR:\tMy info not found :( ");
		}
		
		return null;
	}
	

	@SuppressWarnings("rawtypes")
	static void parseGameInit(String line){
		
			JsonObject json = (JsonObject) new JsonParser().parse(line);
			Debug.print(json.toString());

			json=(JsonObject) json.get("data");
			Debug.print(json.toString());
			
			json= (JsonObject) new JsonParser().parse(json.toString());
			Debug.print(json.toString());

			json=(JsonObject) json.get("race");
			Debug.print(json.toString());
			
			try{
				JsonObject raceSession=(JsonObject) json.get("raceSession");
				Debug.print(raceSession.toString());
		
				Global.laps = Integer.parseInt((((JsonObject) raceSession).get("laps")).toString());
				
			}catch(Exception e){
				Debug.print3("[PARSER]\tCan't get lap count");
				Global.laps=666;
			}
			
			// car stuff
			Global.cars = new ArrayList<Car>(); // reset (for loop testing)
			double[] carData = new double[3];
			JsonArray array=  (JsonArray) json.get("cars");
			Debug.print(array.toString());
			JsonObject id, temp;
			String name;
			
			Iterator iterator = array.iterator();
			while(iterator.hasNext()){
				temp=(JsonObject) iterator.next();
				id =  (JsonObject) temp.get("id");
				name = id.get("name").toString();
				name=name.substring(1,name.length()-1);
				
				temp=(JsonObject) temp.get("dimensions");
				carData[0] = Double.parseDouble(temp.get("length").toString());
				carData[1] = Double.parseDouble(temp.get("width").toString());
				carData[2] = Double.parseDouble(temp.get("guideFlagPosition").toString());
				
				Global.cars.add(new Car(name, carData));
			}
			
			// end of car stuff
			
			if(Global.track.size()==0){
				// track stuff begins
				json=(JsonObject) json.get("track");
				Debug.print(json.toString());
				
				// start of lane stuff
				JsonElement lanes=json.get("lanes");
				Debug.print(lanes.toString());
				
				array= lanes.getAsJsonArray();
				
				Debug.print(array.toString());
				Debug.print("Lane amount: " + array.size());
				
				Global.laneDistances=new double[array.size()];
				Global.laneLengths=new double[array.size()];
				
				iterator = array.iterator();
				while(iterator.hasNext()){
					
					temp=(JsonObject) iterator.next();
					int index=Integer.parseInt(temp.get("index").toString());
					double distance=Double.parseDouble(temp.get("distanceFromCenter").toString());
					Global.laneDistances[index]=distance;
					Debug.print("lane " + index + ": " + distance);
				}
				// end of lane stuff
				
				
				// start of piece stuff
				JsonElement pieces=json.get("pieces");
				Debug.print(pieces.toString());
				
				array= pieces.getAsJsonArray();
	
				for(int i=0; i<Global.laneLengths.length; i++){
					Global.laneLengths[i]=0;
				}
				
				double PI=Math.PI;
				iterator = array.iterator();
				while(iterator.hasNext()){
					double a = 0,r = 0;
					boolean s = false;
					temp=(JsonObject) iterator.next();
		
					double[] lengths=new double[Global.laneDistances.length];
					
					try{	
						
						double length=Double.parseDouble(temp.get("length").toString());
						
						for(int i=0; i<Global.laneDistances.length; i++){
							lengths[i]=length;
							Global.laneLengths[i]+=length;
						}
	
					}catch(Exception e){
						try { a=Double.parseDouble(temp.get("angle").toString()); }catch(Exception e1){}
						try { r=Double.parseDouble(temp.get("radius").toString()); }catch(Exception e2){}
	
						
						if(a>0){
							for(int i=0; i<Global.laneDistances.length; i++){
								double ltemp=(a/360)*2*PI*(r-Global.laneDistances[i]);
								Global.laneLengths[i]+=ltemp;
								lengths[i]=(a/360)*2*PI*(r-Global.laneDistances[i]);
							}
						}else{
							double tempAlpha=Math.abs(a);
							for(int i=0; i<Global.laneDistances.length; i++){
								double ltemp=(tempAlpha/360)*2*PI*(r+Global.laneDistances[i]);
								Global.laneLengths[i]+=ltemp;
								lengths[i]=ltemp;
							}
						}
						
					}
					try{	s=Boolean.parseBoolean(temp.get("switch").toString()); }catch(Exception e){}
		
					
					Global.track.add(new Piece(lengths, r, a, s));	
					
				}
							
				double templength=0;
				for(int i=0; i<Global.laneLengths.length; i++){
					if(Global.laneLengths[i]>templength){
						templength=Global.laneLengths[i];
					}
				}
				
				Global.trackLength=templength;
				
				Debug.print3("Piece amount:\t" +  Global.track.size() );
				
	        	lib.findLongestStraight();
			}

		}	  	
	
	
}
