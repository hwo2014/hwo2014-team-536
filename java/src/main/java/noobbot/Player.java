package noobbot;

import java.util.ArrayList;
import java.util.List;

public class Player {

	private String name;
	private int pieceIndex;
	private double inPieceDistance, angle;
	private boolean notAI; 
	private double speed=0;
	private int lastTick=0;
	private int startLane=0;
	private int spawningTick=0;
	private int crashedAtTick=-400;
	private int endLane=0;
	
	private int crashCount=0;
	
	private List<Double> speeds = new ArrayList<Double>();
	private List<Double> speeds2 = new ArrayList<Double>();

	private double terminalVel=10;
	
	private double predictedAngle=0;
	private double angularAcceleration=0;
	private double angularVelocity=0;
	private double centripetalForce=0;

	private double inCornerAngle=0;
	private double slipLength=0;
	
	private double slipSpeed=0;
	private double slipSpeedPredicted=0;
	
	private double slipForce=0;
	
	private double backMass=10;
	private double carBackLength=30;
	
	private double cornerAngle=0;
	
	private int carID=-1;
	
	private double[] XY = new double[4];
	private double acceleration=0;
	private double throttle=0;
	
	public void resetData(){
		this.lastTick=0;
		this.speed=0;
		this.angularVelocity=0;
		this.crashCount=0;
		this.acceleration=0;
		this.throttle=0;
		this.centripetalForce=0;
		this.lap=1;
//		this.inPieceDistance=0;
		this.angle=0;
//		this.pieceIndex=0;
		this.crashed=false;
		this.hasFinished=false;
		this.speeds2=  new ArrayList<Double>();
		this.turboEnd=0;
		this.turboData[0] =0;
		this.turboData[1] = 1;
		this.turboData[2] = 0;
		this.predictedAngle=0;
		this.laptick=0;
		this.spawningTick=0;
		this.crashedAtTick=-400;
		this.terminalVel=1/Global.drag;
	}
	
	public void reset2(){

		this.terminalVel=1/Global.drag;
		this.speed=0;
		this.angularVelocity=0;

		this.acceleration=0;
		
		this.centripetalForce=0;
		this.slipForce=0;
		this.slipSpeed=0;

		this.angle=0;
		this.predictedAngle=0;
		this.turboEnd=0;
		this.turboData[0] =0;
		this.turboData[1] = 1;
		this.turboData[2] = 0;
		
		this.predictedAngle=0;
		this.slipSpeedPredicted=0;
	}
	
	void setThrottle(double amount){
		this.throttle=amount*(turboLeft() ? this.turboData[1] : 1);
	}
	
	public double getAccel() { return this.acceleration; }
	
	public double getThrottle() { return this.throttle; }
	
	public int crashCount() { return this.crashCount; }
	
	private boolean crashed=false;

	private boolean hasFinished=false;
	private int lap=1;
	private int laptick=0;

	private int turboEnd=0;
	
	private double[] turboData=new double[3]; // #0 = availability, #1 = factor, #2 = duration

	public int getLap(){	return this.lap; }
	
	public void setLap(int lap){ this.lap=lap;	}
	
	public void addLap(int tick){
		
		if(tick>this.laptick){
			if(this.lap+1>Global.laps){
				this.hasFinished=true;
			}else{
				this.lap++;
			}
			this.laptick=tick;
		}

	}
	
	public void setTick(int tick){ this.lastTick=tick; }
	
	public int getLastTick(){	return this.lastTick;}
	
	public void setTurbo(boolean availability, double factor, int duration){
		
		this.turboData[0] = (availability ? 1 : 0);
		this.turboData[1] = factor;
		this.turboData[2] = duration;
	}
	
	public void useTurbo(){
		if(this.turboData[0] != 0){
			this.turboEnd=Global.gameTick +(int)this.turboData[2] ;
			Debug.print("[PHYSICS]\tTerminal vel:" + this.terminalVel);
			this.terminalVel=(1*this.turboData[1])/Global.drag;
			Debug.print("[PHYSICS]\tTerminal vel. with turbo x" + this.turboData[1] + ":" + this.terminalVel);
			this.turboData[0] =0;
		}else{
			Debug.print("[WARNING]\tTried to use turbo twice, bug in code");
		}
	}
	
	public int throttleFactor(){	return (turboLeft() ? (int)this.turboData[1] : 1);	}
	
	public double[] getTurbo(){
		turboLeft();
		return turboData;
	}
	
	public boolean turboLeft(){ // in use and some left
		
			if(Global.gameTick<this.turboEnd){
				return true;
			}else if(turboEnd!=0){ // reset
				this.terminalVel=1/Global.drag;
				Debug.print("[PHYSICS]\tTerminal vel. reseted");
				turboEnd=0;
			}
			
			return false;
	}
	
	public Player(String n,int pidx, double ipd, double a, boolean notAI){
		this.name=n;
		this.pieceIndex=pidx;
		this.inPieceDistance=ipd;
		this.angle=a;
		this.notAI=notAI;
	}
	
	public Player(){
		this.name=null;
		this.pieceIndex=0;
		this.inPieceDistance=0;
		this.angle=0;
		this.notAI=false;
	}
	
	public Player(String n, boolean notAI){
		this.name=n;
		this.pieceIndex=0;
		this.inPieceDistance=0;
		this.angle=0;
		this.notAI=notAI;
	}
	
	boolean skipTick=false;
	
	public void setSpeed(double inPiece,int pieceID, int tick){

		if(this.crashed || this.hasFinished) return;
		
			double journey;
			if(pieceID!=this.pieceIndex){
				journey=Global.track.get(this.pieceIndex).getLength(this.startLane, this.endLane)-this.inPieceDistance;
				journey+=inPiece;
//				Debug.print("pieceID: " + pieceID + " , this.pieceIndex: " + this.pieceIndex);
			}else{
				journey=inPiece-this.inPieceDistance;
			}
			
			
			double tempspeed = journey/(tick-this.lastTick);
			
			if(!this.notAI){
				
				if(!skipTick){
					this.speeds2.add(tempspeed);
					if(this.speeds2.size()>176) this.speeds2.remove(0);	
				}
				
				skipTick^=true;
				double deltaV=tempspeed-this.speed;
				double deltaT=tick-this.lastTick;
				if(deltaT>0){
					this.acceleration=deltaV  / deltaT ;
					
				}
				
				if(Global.speedsFound==false && inPiece>0){
					
					this.speeds.add (tempspeed);
					Debug.print("[PHYSICS]\tTick: " + tick + " , speed: " + this.speeds.get(this.speeds.size()-1));
					
					if(this.speeds.size()>2) Global.speedsFound=true;
				}
			}
			
			this.speed=tempspeed;
			
			/*
			if(tempspeed>1.2){
				Debug.print("piecelength: " + Global.track.get(this.pieceIndex).getLength(this.endLane));
				Debug.print("piecelength prev: " + Global.track.get(this.pieceIndex).getLength(this.startLane, this.endLane));
				Debug.print("this.lastTick: " + this.lastTick);
				Debug.print("tick: " + tick);
				Debug.print("inPiece: " + inPiece);
				Debug.print("pieceID: " + pieceID);
				Debug.print("this.inPieceDistance: " + this.inPieceDistance);
				Debug.print("journey: " + journey);
				Debug.print("acceleration: " + this.acceleration);
				Debug.print("speed: " + this.speed);
				System.exit(0);
			}
			*/
		
	}
	
	public int getEndLane(){ return this.endLane; }
	
	public int getStartLane(){	return this.startLane; }
	
	public  void spawn(){	
		this.crashed=false; 
		this.speeds2=  new ArrayList<Double>();
		Global.spawningTime=Global.gameTick-this.crashedAtTick;
	}
	
	public  void crash(boolean dnf){
		this.crashed=true;
		this.crashedAtTick=Global.gameTick;
		this.spawningTick=Global.gameTick+Global.spawningTime;

		if(!dnf){
			if( this.centripetalForce<Global.maxForce && this.centripetalForce>Global.mass && this.acceleration<=Global.acceleration){
				Global.maxForce=this.centripetalForce*0.9999;	
				Debug.print3("[PHYSICS]\tNew max. centripetal force:\t" + Global.maxForce + " learned from: " + this.name);
				lib.findTravelTimes();
				Global.physicsFound=true;
			}else if(this.notAI==false){
				if(AI.ramming==false){ // not caused by bugged ramming
					Global.physicsFound=true;
					Global.maxForce*=turboLeft() ? 0.99 : 0.975; // still learn a little and even less if we failed with turbo
					Debug.print3("[PHYSICS]\tForce before crash:\t" + this.centripetalForce + " learned from: " + this.name);	
					Debug.print3("[PHYSICS]\tNew max. centripetal force:\t" + Global.maxForce + " learned from: " + this.name);
				}
			}
			
			
	/*		if(this.physicsFound 	){
				
	        	Debug.print("maxForce:\t" + Global.maxForce);
	        	
				Debug.print("[PHYSICS]\tDrag:\t" + Global.drag);
				
				Debug.print("[PHYSICS]\tMass:\t" + this.mass());
				
				Debug.print("[PHYSICS]\tMax speed:\t" + this.terminalVel());
				
				System.exit(0);
			}*/
			
			
	
			Debug.print("[PHYSICS]\tSlipforce before crash:\t" + this.slipForce);

			Global.maxAngle=(Math.abs(this.angle)+Global.maxAngle)/2;
			Debug.print3("[PHYSICS]\tMax. slipangle:\t" + Global.maxAngle + " learned from: " + this.name);
			Global.maxSlipLength=(Global.maxAngle*Math.PI*this.carBackLength)/180;
			Global.slipRatio=Global.maxSlipLength/this.carBackLength;
			
			
			this.crashCount++;
		}
		
		this.reset2();
	}
	
	public  void finish(){	this.hasFinished=true;	}
	
	public boolean finished(){ return this.hasFinished;	}
	
	public boolean crashed(){ return this.crashed;	}
	
	public double speed(){ return this.speed;	}

	public void setEndLane(int value){ this.endLane=value; }
	
	public void setStartLane(int value){ this.startLane=value; }

	public void setPieceDist(double dist){	this.inPieceDistance=dist;	}
	
	private double[] previousCorner=new double[]{0,0,0,0,0,0}, tempCorner=new double[]{0,0,0,0,0,0};
	
	public void maths(double angle, int tick){

		if(this.crashed==true || this.hasFinished==true) return;
		
		if(this.carID<0){
			this.carID = lib.findCar(this.name);
			this.carBackLength=Global.cars.get(this.carID).backLength();
			Global.maxSlipLength=(Global.maxAngle*Math.PI*this.carBackLength)/180;
			Global.slipRatio=Global.maxSlipLength/this.carBackLength;
		}
			
		if(!Global.dragAndMassFound && Global.speedsFound && this.notAI==false){

			double v1=this.speeds.get(0), v2=this.speeds.get(1), v3= this.speeds.get(2);
			
			Global.drag = (v1 - (v2 - v1)) / Math.pow(v1, 2) * this.throttle;
			Debug.print3("[PHYSICS]\tDrag:\t" + Global.drag);
			
			Global.mass = 1 / (lib.ln (( v3 - (this.throttle / Global.drag)) / (v2 -(this.throttle/Global.drag)  ) )  / -Global.drag );
			Debug.print3("[PHYSICS]\tMass:\t" + Global.mass);
			Global.dragAndMassFound=true;
			
			this.backMass=carBackLength/Global.cars.get(this.carID).length();
			this.backMass*=Global.mass;
			Debug.print3("[PHYSICS]\tCar back mass: " + this.backMass);
			
			this.terminalVel=1/Global.drag;
			Debug.print3("[PHYSICS]\tMax. speed:\t" + this.terminalVel);
			
			Global.defaultMaxSpeed=this.terminalVel;

			Global.minAvgSpeed=Global.drag*470/Global.mass;
			Debug.print3("[PHYSICS]\tMin avg. speed:\t" + Global.minAvgSpeed);
			
			Global.maxForce=Global.mass*Global.drag*17.5;
			Debug.print3("[PHYSICS]\tMax force estimation:\t" + Global.maxForce);
			
			Global.acceleration=v1;
			Debug.print3("[PHYSICS]\tAcceleration:\t" + Global.acceleration);
			
        	lib.findTravelTimes();
        	
        	Debug.printTrack();	  
			
		}else{

			int dTime=tick-this.lastTick;
			
			double newAngularVelocity=(angle-this.angle)/dTime;
			
			double temp= (newAngularVelocity-this.angularVelocity)/dTime;
			if(Math.abs(temp)<50) this.angularAcceleration = temp; // TODO: fix these
			if(Math.abs(newAngularVelocity)<50)  this.angularVelocity = newAngularVelocity;
			this.predictedAngle=angle+(this.angularVelocity*1.05+this.angularAcceleration*1.05);
			
//			this.slipLength=(this.angle/360)*2*Math.PI*Global.cars.get(this.carID).length();

			double[] corner = lib.getCorner(this.pieceIndex, this.startLane, this.endLane, this.inPieceDistance, true, true);  //length, ang, r, inpiece

			double newSlipSpeed=(this.angularVelocity()/360)*2*Math.PI;
			temp=(newSlipSpeed-this.slipSpeed)/dTime;
			this.slipSpeedPredicted=newSlipSpeed+temp;
			this.slipSpeed=newSlipSpeed;
			
//				Debug.print("slipSpeed: " + slipSpeed + " angularvel: " + this.angularVelocity + " slipang: " + this.angle);
			this.slipForce = this.backMass*Math.cos(Math.toRadians(90-Math.abs(angle)))*(angle<0?-1:1) + this.backMass*Math.pow(this.slipSpeedPredicted, 2)/this.carBackLength;
//			this.slipForce =(this.angularAcceleration*this.backMass);
//			this.slipForce*=angle<0 ? -1 : 1;
			
//				Debug.print("slipForce: " + slipForce);
			
			if(corner!=null){
				double pieceRadius=corner[2];
				
//				this.centrifugalForce=Math.pow(this.speed, 2)/corner[2];
				
				this.inCornerAngle=360*(corner[3]/(2*Math.PI*pieceRadius));
				
				if(Config.vis && this.notAI==false){

//						double cornerAngle=360*((corner[1]<0 ?  corner[3] : corner[0]-corner[3]  )/(2*Math.PI*pieceRadius));
					
					this.XY[0] = pieceRadius*Math.cos(Math.toRadians(180-this.inCornerAngle));
					this.XY[1] = pieceRadius*Math.sin(Math.toRadians(180-this.inCornerAngle));
					
					double backangle=Math.toRadians(this.angle-(90-(90-Math.abs(corner[1])-this.inCornerAngle)));
				
					this.XY[2] = Math.cos(backangle)*this.carBackLength;
					this.XY[3] = Math.sin(backangle)*this.carBackLength;
	
					this.XY[2] += this.XY[0];
					this.XY[3] += this.XY[1];	

				}

				boolean sameway=lib.isSameWay(this.tempCorner[1],corner[1]);
				if(this.tempCorner[2]!=corner[2] || (!sameway || corner[1]==0)){ // radius or corner way is different, must be new corner here so lets set the previous corner as previous..
					this.previousCorner=this.tempCorner;
				}
				
				this.tempCorner=corner;
				 sameway=this.previousCorner[1]!=0 ? lib.isSameWay(this.previousCorner[1],corner[1]) : false;
				 
				this.cornerAngle=corner[1]+(sameway ? this.previousCorner[1] : 0);
				this.inCornerAngle+=(sameway ? Math.abs(this.previousCorner[1]) : 0);

				double angleFromCenter=Math.abs(90 - (this.inCornerAngle > 90 ? 90 : this.inCornerAngle));
				
				Debug.print2("angleFromCenter: " + angleFromCenter + " & predictedAngle: " + this.predictedAngle);
				
				this.centripetalForce = Global.mass*Math.cos(Math.toRadians(angleFromCenter)) +  Global.mass*Math.pow(this.speed, 2)/pieceRadius;

				if(!this.notAI) {
					if(this.acceleration<0 && this.crashCount<this.lap){
						
						if(angleFromCenter<(Global.physicsFound ? 2: 10) && Math.abs(this.predictedAngle)<Global.maxAngle*(Global.physicsFound ? 0.25 : 0.666)){ 
							Global.maxForce+=Global.physicsFound ? 0.005 : 0.02;
							AI.minSpeed*=Global.physicsFound ? 1.00175: 1.01;
							Debug.print3("[PHYSICS]\tGoing too slow... maxF -> " + Global.maxForce + " and AI.minSpeed -> " + AI.minSpeed);
						}
					}else if(!skipTick && this.speeds2.size()>170){
						if(this.speed<Global.minAvgSpeed ){
							double avgspeed= lib.avg(this.speeds2);
							Debug.print3("[AI]\t250 ticks avg. speed is: " + avgspeed);
							
							if(avgspeed<Global.minAvgSpeed){
								Global.maxForce+=0.045;
								AI.minSpeed*=1.075;
								Debug.print3("[PHYSICS]\tTrying to go faster... maxF -> " + Global.maxForce + " and AI.minSpeed -> " + AI.minSpeed);
							}
						}
					}
				}
				
				if(angleFromCenter<5 && Global.dragAndMassFound ){
					if(this.centripetalForce>0 && Math.abs(this.predictedAngle)>=Global.maxAngle){ // learn from too high future slipangle
						if((Global.maxForce>this.centripetalForce || !Global.physicsFound) && this.centripetalForce>Global.mass){
							Debug.print3("[PHYSICS]\tNew max. centripetal force:\t" + this.centripetalForce + " learned from: " + this.name + " by slip angle of " + angle + " (-> " + (angle+this.angularVelocity+this.angularAcceleration) + ")");
							if(!Global.physicsFound){
								Global.maxForce=this.centripetalForce;
							}else{
								Global.maxForce=(Global.maxForce*3+this.centripetalForce)/4;
							}

							lib.findTravelTimes();
						}

					}else if(this.centripetalForce<Global.maxForce && angle>Global.maxAngle/2 && !Global.physicsFound){
						Global.maxForce += Global.mass*Math.cos(Math.toRadians(angleFromCenter)) +  Global.mass*Math.pow(Global.maxAngle/angle*this.speed, 2)/pieceRadius;
						Global.maxForce/=2;
						
						Debug.print3("[PHYSICS]\tNew max. centripetal force:\t" + Global.maxForce + " learned from: " + this.name + " by slip angle of " + angle + " / " + Global.maxAngle);

					}
				}
				
			}else{
				 this.cornerAngle=0;
				this.centripetalForce=0;
				this.previousCorner=new double[]{0,0,0,0,0,0};
				this.tempCorner=new double[]{0,0,0,0,0,0};
				this.inCornerAngle=0;
			}
			
		}	

		this.angle=angle;
	

		
	}

		
	public double angularAcceleration() { return this.angularAcceleration; }
	
	public double angularVelocity() { return this.angularVelocity; }
	
	public double inCornerAngle() { return this.inCornerAngle; }
	
	public double cornerAngle() { return this.cornerAngle; }
	
	public boolean thirdLeft(){
		if(this.inCornerAngle>this.cornerAngle*0.66) return true;
		
		return false;
	}
	
	public boolean halfLeft(){
		if(this.inCornerAngle>this.cornerAngle/2) return true;
		
		return false;
	}

	public double centripetalForce() { return this.centripetalForce; }
		
	public double slipLength() { return this.slipLength; }
	
	public double slipSpeed() { return this.slipSpeed; }
	
	public double slipSpeedPredicted() { return this.slipSpeedPredicted; }
	
	public double slipForce() { return this.slipForce; }
		
	public double terminalVel() { return this.terminalVel; }
	
	public double predictedAngle() { return this.predictedAngle; }
	
	public boolean endOfCorner(){
		if(this.cornerAngle>=180){
			return this.cornerAngle-this.inCornerAngle<90? true : false;
		}else{
			return this.cornerAngle-this.inCornerAngle<45 ? true : false;
		}
		
	}
	
	public boolean safeEnd(){
		if(this.cornerAngle>=180){
			return this.cornerAngle-this.inCornerAngle<Global.maxAngle*0.75? true : false;
		}else{
			return this.cornerAngle-this.inCornerAngle<Global.maxAngle*0.25 ? true : false;
		}
		
	}


	public void setPieceIndex(int pieceIndex){	this.pieceIndex=pieceIndex; }
	
	public double pieceDist(){ return this.inPieceDistance; }
	
	public int[] carXY(){
		return new int[] {(int) this.XY[0],(int) this.XY[1],(int) this.XY[2],(int) this.XY[3], (int)Global.cars.get(this.carID).width(), this.crashed ? 1 : 0, (int) this.inCornerAngle};
	}
	
	public double X(){ return this.XY[0]; }
	public double Y(){ return this.XY[1]; }
	
	public double X2(){ return this.XY[2]; }
	public double Y2(){ return this.XY[3]; }
	
	public double angle(){ 	return this.angle; }
	
	public double acceleration(){ 	return this.acceleration; }
	
	public int pieceIdx(){		return this.pieceIndex;	}
	
	public int spawningAt(){		return this.spawningTick;	}
	
	public int spawningIn(){		return this.spawningTick-Global.gameTick;	}
	
	public String name(){		return this.name;	}

	public boolean stupid(){ 	return this.notAI; }
}
