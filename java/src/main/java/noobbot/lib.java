package noobbot;

import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class lib {
	
	public static void findTravelTimes(){
		for(int x=0; x<Global.laneDistances.length; x++){
			AI.findMaxSpeed(0, x, true);
			
		}
	}
		
	public static void setTravelTimes(int lane, double startpiece, double endpiece, double avgspeed){
		int temp=(int) startpiece;
		Debug.print2("setting travletimes from " + startpiece + " to " + endpiece + " speed: " + avgspeed);
		
		for(int i = 0 ; i<Global.track.size(); i++){
			Global.track.get(temp).setTravelTime(lane, avgspeed);
			
			if(temp==endpiece) break;
			
			temp=nextPiece(temp,1);
		}
	}
	
	public static boolean isBetweenPieces(int piece, int start, int end){
		int piecetemp=piece, i;
		int size=Global.track.size();
		
		if(start>end){
			int temp=end;
			end=start;
			start=temp;
		}
		
		if(piece!=start){
			for(i =1;i<size; i++){
				piecetemp=previousPiece(piecetemp,1);
				if(piecetemp==start) break;
			}
		}else{
			piecetemp=piece;
			i=0;
		}
				
		if(size/2-1<i) return false;
		
		if(piece!=start){
			piecetemp=piece;
			for(i =1;i<size; i++){
				piecetemp=nextPiece(piecetemp,1);
				if(piecetemp==end) break;
			}
		}else{
			piecetemp=piece;
			i=0;
		}
		
		if(size/2-1<i) return false;
		
		return true;
		
	}
	
	public static void findLongestStraight(){
		
		List<int[]> straights = new ArrayList<int[]>();
		int start=-1, end=-1, longest = 0, longestDist=0;
		double cornerAfter=999, tightness;
		
		boolean sameway=true;
		
		for(int i=0; i<Global.track.size()*2; i++){
			
			int piece=nextPiece(0,i);
//			Debug.print("piece: " + piece);
			
			double[] corner = getCorner(piece, 0,0,0, true, false);
			
			if(corner==null){
				tightness = 0; // straight	
				sameway=true;
			}else{
				tightness=Math.abs(corner[1]/corner[2]);
				double prevAng=previousPiece(piece,1);
				sameway=isSameWay(prevAng,corner[1]);
			}
			
//			Debug.print("tigtness1 " + tightness);
			

			if(tightness<0.215){
				if(start<0 && sameway && Global.track.get(nextPiece(piece,1)).getAngle()==0){
					Debug.print("starttightness: " + tightness);
					start=piece;
				}
			}else{
				end=piece;
				if(start>-1){
					int dist=distToPiece2(start,end, false);
					straights.add(new int[]{start ,(int) (tightness*100), dist});
					Debug.print("[PARSER]\tStraight found: " + start + " - " + end + " = " + dist);
					Debug.print("endtightness: " + tightness);
					
					if(dist>longestDist || (dist>=longestDist*0.85 && cornerAfter > tightness)){
//						Debug.print("SStigtness " + tightness);
						longestDist=dist;
						cornerAfter=tightness;
						longest=straights.size()-1;
					}
					
				}
				start=-1;
				end=-1;
			}
			
		}
		
		if(longestDist<150){
			Debug.print("[PARSER] Longest straight is not long enough.");
			Global.straight=new int[]{-1, -1};
		}
		
		int longestTightness=straights.get(longest)[1];
		int longestDistance=straights.get(longest)[2];
		int longest2=longest;
		
		for(int i=0; i<straights.size(); i++){
			if(i!=longest){
				if(straights.get(i)[2]==longestDistance && straights.get(i)[1]==longestTightness){
					longest2=i;
					break;
				}
			}
			
		}
		Global.straight=new int[]{straights.get(longest)[0], straights.get(longest2)[0]};
		
		Debug.print("cornerAfter: " + cornerAfter);
		Debug.print("[PARSER]\tLongest straights start at: " + Global.straight[0] + " & " + Global.straight[1] );

	}
	
	public  static double round( double num, int multipleOf) {
  	  return Math.round((num + multipleOf/2) / multipleOf) * multipleOf;
  	}
    
    public static double avg(List<Double> valueList){
    	
    	double sum=0;
    	
    	for(int i=0; i<valueList.size(); i++){
    		sum+= valueList.get(i);
    	}
    	
    	return sum/valueList.size();
    	
    }
    
	public static boolean isSameWay(double a1, double a2){
		
		if((a1<0 && a2<0) || (a1>0 && a2>0)) return true;
				
		return false;
		
	}
    
    public static boolean isBetween(double a, double b, double c) {
        return b > a ? c > a && c < b : c > b && c < a;
    }
    
	public static void reset(){
		AI.targetSpeed=99;
		Global.gameTick=0;
		 AI.laneDistances = new int[3];
		 AI.laneSpeeds=new double[3];
		AI.switchingAt=-1;
	    AI.lastPiece=-1;
		
	    AI.straightAmount=0;
	    AI.straightPieces=0;
		AI.switchedAtPiece=-1;
    	AI.lastStraight=false;
    	AI.ramming=false;
    	AI.lastPiece=-1;
    	
    	for(int i =0; i< Global.players.size(); i++){
    		Global.players.get(i).resetData();
    	}
    }
	
	public static void hardReset(){
		
		Global.straight = new int[2];
		
    	Global.drag=0.1;

    	Global.maxForce=20;
    	
    	Global.spawningTime=400;
    	
    	Global.minAvgSpeed=5;
    	
    	Global. mass=10;
    	
    	Global.maxAngle=60;
    	
    	Global.maxSlipLength=30;
    	
    	Global.slipRatio=1.5;
    	
    	Global.acceleration=0.2;
    	
    	Global.defaultMaxSpeed=1/Global.drag;
    		
    	Global.physicsFound=false;
    	
    	Global.dragAndMassFound=false;

    	Global.speedsFound=false;

    	Global.track = new ArrayList<Piece>();
    	Global.players = new ArrayList<Player>();
    	Global.cars = new ArrayList<Car>();
    	
    	Global.laneDistances=new double[2];
    	
    	Global.laneLengths=new double[2];
    	
    	Global.trackLength=0;
	}
    
	public static double ln(double input){		return Math.log(input)/Math.log(10);	}
	
    public static double brakingDistance(double targetSpeed, int playerID){
    	
    	if(targetSpeed==0) targetSpeed=0.5;

    	double speed=Global.players.get(playerID).speed();
		double mass=Global.mass;
		
    	double ticks =  Math.ceil( (ln (
			    												targetSpeed   / speed  ) 
			    												* mass ) 
			    												/ -Global.drag);
    	
//    	Debug.print("ticks: " +ticks);
    	
    	double distance = (mass / Global.drag) * speed  * (1- Math.pow(Math.E , -Global.drag*ticks / mass ));
//    	Debug.print("distance: " + distance);
    	
    	return (distance);
    }
	
	public static int findCar(String owner){
		for(int i=0; i<Global.cars.size(); i++){
			if(Global.cars.get(i).owner().equals(owner)) return i;
		}
		
		Debug.print("Car of " + owner + " not found!");
		Debug.printCars();
		return -1;
	}
	
	
	static int previousPiece(int thisPiece, int length){
		
		int next;

		next = thisPiece-length;
		return (next<0 ? next+Global.track.size() : next);
	
	}
	
	static int nextPiece(int thisPiece, int length){
		
		int next;

			next = thisPiece+length;
			return (next>Global.track.size()-1 ? next-Global.track.size() : next);
		
	}
		
	static int distToPiece(int thisPiece, int thePiece){
		
		if(thisPiece<thePiece) return thePiece-thisPiece;
		
		if(thisPiece>thePiece) return Global.track.size()-thisPiece+thePiece;
		
		return 0;
	}
	
	static int distToPiece2(int thisPiece, int thePiece, boolean between){
		Debug.print2("thisPiece" + thisPiece + " , thePiece: " + thePiece);
		double distance=0;
		
		if(between){
			thisPiece=nextPiece(thisPiece,1);
			thePiece=previousPiece(thePiece,1);
		}
		
		for(int i=0; i<Global.track.size(); i++){
			
			distance+=Global.track.get(thisPiece).getLength();
			Debug.print2("dist: " + distance + " , thispiece: " + thisPiece);
			
			if(thisPiece==thePiece) break;
			
			thisPiece=nextPiece(thisPiece,1);
			
		}

		return (int)Math.round(distance);
		
	}

	
	
	public static int listIDbyName(String name){

//		Debug.print("player list size: " + Global.players.size());
		
		for(int i=0; i<Global.players.size(); i++){
			
			if(Global.players.get(i).name().equals(name)) return i;
		}
		
		Debug.print("Player name not found");
		return -1;
	}
	
	static boolean skipTick=true;
	
	static void playerInfo(){
		
		skipTick^=true;
		
		if(!skipTick){
		
			Debug.print("Game tick:\t\t" + Global.gameTick +"\n");
			
			for(int i=0; i<Global.players.size(); i++){
				
				Debug.print("\t" + Global.players.get(i).name()  + (Global.players.get(i).crashed() ? " (crashed, spawning in " + (Global.players.get(i).spawningAt()-Global.gameTick) + ")" : "")  + (Global.players.get(i).finished() ? " (finished)\n" : 
			    		"\n\t\tSpeed:\t" + (int)Math.round(Global.players.get(i).speed())
			    		+ "\n\t\tAngle:\t" + (int)Math.round(Global.players.get(i).angle())
						+ "\n\t\tLap:\t" + Global.players.get(i).getLap() + " / " + Global.laps
						+"\n\t\tPiece:\t" +Global.players.get(i).pieceIdx() + " / " + (Global.track.size()-1) 
						+"\n\t\tCrash count:\t" +Global.players.get(i).crashCount() + "\n"));
			}
		}
	}
	
	public static void statusreport(Player player){
   		if(player.crashed() || player.finished()){
			playerInfo();
   		}else{
//    		double speed=player.speed();
    		int startlane=player.getStartLane();

    		Debug.print("" 
//    				"\tSpeed:\t\t\t" + (double)Math.round(speed*10)
//	    		+"\n\tAcceleration:\t\t" + player.getAccel() 
//	    		+"\n\tThrottle:\t\t" + player.getThrottle()
//	    		+"\n\tTurbo available:\t" + (player.getTurbo()[0]==1 ? "true" : "false") 
//	    		+"\n\tTurbo in use:\t\t" + player.turboLeft()
	    		+"\tSlip angle:\t\t" + player.angle() + " / " + player.predictedAngle()
	    		+"\n\tSlip force:\t\t" + player.slipForce()
//	    		+"\n\tAngular acceleration:\t" + player.angularAcceleration()
//	    		+"\n\tAngular velocity:\t" + player.angularVelocity()
//	    		+"\n\tIn corner X:\t" + player.X()
//	    		+"\n\tIn corner Y:\t" + player.Y()
//	    		+"\n\t\tback X:\t" + player.X2()
//	    		+"\n\t\tback Y:\t" + player.Y2()
	    		+"\n\tCentripetal force:\t" + player.centripetalForce() + " / " + Global.maxForce
//	    		+"\n\tCentrifugal force:\t" + player.centrifugalForce()
//    		+"\n\tIn corner angle:\t" + player.inCornerAngle()
	    		+"\n\tLane:\t\t\t" + startlane + (startlane!=AI.bestlane ?  " / " + AI.bestlane +   " (Maybe switching at " + AI.switchingAt + "...)" : "")
	    		+"\n\tLap:\t\t\t" + player.getLap() + " / " + Global.laps
	    		+"\n\tIn corner angle:\t" + (int)Math.round(player.inCornerAngle()) + " / " + (int)Math.round(Math.abs(player.cornerAngle())));
//	    		+"\n\tInPiece dist:\t" + (int)Math.round(player.pieceDist()));
    		
    		int idx=player.pieceIdx();

    		try{
        		Debug.print("\tPiece index:\t\t" + idx + " / " + (Global.track.size()-1));
//    			Debug.print("\tPiece angle:\t" + Global.track.get(idx).getAngle());
//    			Debug.print("\tLane length:\t" + Global.track.get(idx).getLength(startlane, endlane));
//    			Debug.print("\tPiece switch:\t" + Global.track.get(idx).hasSwitch());
    		}catch(Exception e){
    			Debug.print("Statusreport() error: " + e);
    		}
    		Debug.print("");
		}
	}
	
	static boolean first=true;
	public static void mathCSV(int playerID){
		try{
		
			Player temp = Global.players.get(playerID);
		
			if(temp.crashed()) return;
			
			String str = Global.gameTick + ";" + temp.speed()
//					+ ";" +  temp.getAccel()
//					+";" + temp.getThrottle()
					+";" + temp.angle()
					+";" + temp.angularAcceleration()
					+";" + temp.angularVelocity()
					+";" + temp.centripetalForce()
//					+";" + temp.inCornerAngle()
					+";" + Global.track.get(temp.pieceIdx()).getAngle()
					+";" + temp.pieceIdx()
					+";" + Global.track.get(temp.pieceIdx()).getRadius(temp.getStartLane());
//					+";" + temp.slipLength()
//					+";" + temp.X()
//					+";" + temp.Y()
//					+";" + temp.X2()
//					+";" + temp.Y2()
//					+";" + temp.pieceDist()
//					+";" + temp.centrifugalForce()
//					+";" + temp.slipForce();
			
			if(first){
				str+=";" + Global.mass + ";" + Global.drag + ";" + Global.defaultMaxSpeed;
				str="tick;speed;slip angle;angular accel;angular vel; centri F;piece angle; piece ID; piece radius;mass;drag;terminal velocity;\n" + str;
				first=false;
			}
			
			writeLog(str.replace(".",","), "csv/math.csv",false);
		
		}catch(Exception e){ e.printStackTrace(); }
	}

	public static String getCurrentTimeStamp() {
	    SimpleDateFormat sdfDate = new SimpleDateFormat("HH:mm:ss.SSS");//dd/MM/yyyy
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
	    return strDate;
	}
	
	public static String randomTrack(){
		
		int n= new Random().nextInt(9);
		
		switch (n){
			case 0:
				return "keimola";
			case 1:
				return "germany";
			case 2:
				return "usa";
			case 3:
				return "france";
			case 4:
				return "elaeintarha";
			case 5:
				return "imola";
			case 6:
				return "england";
			case 7:
				return "suzuka";
			case 8:
				return "pentag";
		}
		
		return null;
	}
	
	public static void writeLog(String text, String filename, boolean timestamp){
		
		
		FileWriter out = null;
		try {
			out = new FileWriter(filename,true);

			out.write((timestamp ? getCurrentTimeStamp() +": " : "") + text + System.getProperty( "line.separator" ) );

				out.close();
			} catch (Exception e) {

			}
		}
	
    public static boolean canSwitch(int lane, int direction){
    	if(lane<=0 && direction<0) return false;
    	if(lane>=Global.laneDistances.length-1 && direction>0) return false;
    	
    	return true;
    }
	
    static int tolerance=50;
	public static double[] getCorner(int currentPiece, int startlane, int endlane, double inpiece, boolean print, boolean findStart){

		double ang=Global.track.get(currentPiece).getAngle(), radius = Global.track.get(currentPiece).getRadius(endlane);
		
		double low=radius-tolerance, high= radius+tolerance;
		
		double lowestRadius=radius;

		if(ang==0) return null;
		
		Debug.print2("getCorner() tolerance: " + low + " - " + high);
		
		double[] corner = new double[7];
		
		corner[0] = Global.track.get(currentPiece).getLength(startlane, endlane);
		corner[1] = ang;
		corner[2] = radius;
		corner[3] = inpiece;
		corner[6] = 0;
		
		if(findStart){
			// find where to corner started
			for(int i=1; i<Global.track.size(); i++  ){
				
				int tempID=previousPiece(currentPiece,i);
				double tempAng=Global.track.get(tempID).getAngle();
	//			Debug.print("tempID " + tempID);
	//			Debug.print("tempAng " + tempAng);
								
				double temp=Global.track.get(tempID).getRadius(endlane);
				
				if(tempAng!=0 && isBetween(low,high,temp) && isSameWay(ang, tempAng) ){
					double l =Global.track.get(tempID).getLength(startlane);
					corner[0] += l;
					corner[1] += tempAng;
					corner[3] += l;
					if(temp<lowestRadius){
						corner[2]=temp;
						lowestRadius=temp;
					}
				}else{
					corner[4]=nextPiece(tempID,1);
					break;
				}
				
			}
		}
//		Debug.print("-------------\nang " + ang);

		// find where this type of corner ends
		for(int i=1; i<Global.track.size(); i++  ){
			
			int tempID=nextPiece(currentPiece,i);
			double tempAng=Global.track.get(tempID).getAngle();

//			Debug.print("tempID " + tempID);
//			Debug.print("tempAng " + tempAng);

			if(Global.track.get(tempID).hasSwitch()) corner[6]=1;
			double temp=Global.track.get(tempID).getRadius(endlane);

			if(tempAng!=0 && isBetween(low,high,temp) && isSameWay(ang, tempAng) ){
				corner[0 ]+= Global.track.get(tempID).getLength(endlane);
				corner[1] += tempAng;
				if(temp<lowestRadius){
					corner[2]=temp;
					lowestRadius=temp;
				}
			}else{
//				Debug.print("tempID " + tempID);
//				Debug.print("currentPiece " + currentPiece);
				corner[5]=previousPiece(tempID,1);
				break;
			}

		}
//		Debug.print("end " + Global.laneDistances[endlane] + " start" + Global.laneDistances[startlane]);
//		corner[2] +=(corner[1]>0 ? -(Global.laneDistances[endlane] + Global.laneDistances[startlane])/2 : (Global.laneDistances[endlane] + Global.laneDistances[startlane])/2);

		if(print) Debug.print2(corner[0] + "\n" + corner[1] + "\n" + corner[2] + "\n" + corner[3] + "\n"+ corner[4] + "\n" + corner[5] + "\n");
	
		return corner;
	}
	
	
}
