package noobbot;

import java.util.ArrayList;
import java.util.List;


public class Global {

	public static double drag=0.1;

	public static double maxForce=20;
	
	public static int spawningTime=400;
	
	public static double minAvgSpeed=5;
	
	public static double mass=10;
	
	public static double maxAngle=60;
	
	public static double maxSlipLength=30;
	
	public static double slipRatio=1.5;
	
	public static double acceleration=0.2;
	
	public static double defaultMaxSpeed=1/drag;
		
	public static boolean physicsFound=false;
	
	public static boolean dragAndMassFound=false;

	public static boolean speedsFound=false;

	public static List<Piece> track = new ArrayList<Piece>();
	public static List<Player> players = new ArrayList<Player>();
	public static List<Car> cars = new ArrayList<Car>();
	
	public static double[] laneDistances=new double[2];
	
	public static double[] laneLengths=new double[2];
	
	public static double trackLength=0;
	
	public static int gameTick=0;
	
	public static int laps=0;
	
	public static int[] straight = new int[2];
	
}

