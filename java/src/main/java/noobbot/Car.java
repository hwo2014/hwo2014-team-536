package noobbot;

public class Car {

	private String owner;
	private double[] carData = new double[3];
	private double backLength;
	
	public Car(String owner, double[] carData){
		this.owner=owner;
		this.carData =  carData;
		this.backLength=carData[0]-carData[2]; // length-guideflagposition
	}
	
	public String owner() { return this.owner; }
	
	public double length(){	return this.carData[0];	}
	
	public double width(){	 return this.carData[1];	}
	
	public double guideFlagPosition(){ 	return this.carData[2]; }
	
	public double backLength(){ return this.backLength; }
	
}
